import cors from 'cors'
import logops from 'logops'
import redis from 'redis'
import helmet from 'helmet'
import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import expressLogging from 'express-logging'

import initDb from './service/db'

import router from './routes/index'
import config from './config'

const server = express()

server.use(cors())
server.use(helmet())
server.use(cookieParser())
server.use(bodyParser.json({ limit: config.reqBody.limit }))
server.use(expressLogging(logops))

server.use(config.urlMount, router)

initDb()

const serverInstance = server.listen(config.port_listen, config.host, () => {
	logops.info(
		`${config.server.name.toUpperCase()} v${
			config.server.version
		} listening at ${config.host}:${config.port_listen}`
	)
})

let client = redis.createClient()

client.on('connect', function () {
	console.log('connected to redis')
})

process.on('SIGINT', () => {
	logops.info('Stopping server')

	if (serverInstance) {
		serverInstance.close(() => {
			logops.info('Server stopped')
			process.exit(1)
		})
	}
})

export default client
