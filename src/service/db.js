import pg from 'pg'
import config from '../config'

const configPool = {
	user: config.user,
	database: config.database,
	password: config.password,
	host: config.host,
	port: config.port,
}

export default function initDb() {
	const pool = new pg.Pool(configPool)
	pool.connect(function (err, client, done) {
		if (err) {
			console.log('Can not connect to the DB' + err)
			client.end() // close the connection
		} else {
			console.log('successfully connected')
		}
	})
}
