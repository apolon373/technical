import joi from 'joi'
const _ = require('lodash')
import logger from 'logops'

const rules = {
	createUser: joi.object({
		full_name: joi.string().required(),
		date_of_birth: joi.date().required(),
		gender: joi.string().required().valid('male', 'female'),
	}),
	updateUser: joi.object({
		full_name: joi.string().required(),
		date_of_birth: joi.date().required(),
		gender: joi.string().required().valid('male', 'female'),
	}),
}

function getJoiErrorParamName(joierr) {
	const wrongParamPath = _.get(joierr, 'details[0].path', [])
	return Array.isArray(wrongParamPath) ? wrongParamPath.join(' ') : ''
}

const exportObj = {}
_.forEach(rules, (value, key) => {
	exportObj[key] = (req, res, next) => {
		if (!(key in rules)) {
			const error = 'Validation rule not found'
			res.status(500).json({ success: false, error })
			return next(error)
		}

		const data = req.method === 'GET' ? req.query : req.body

		if (_.keys(rules[key]).length > 0 && (!data || _.keys(data).length === 0)) {
			const error = 'Empty payload'
			logger.warn(`Validation eror: ${error}`)
			res.status(400).json({ success: false, error })
			return next(error)
		}

		joi.validate(data, rules[key], (joierr, payload) => {
			if (joierr) {
				const field = getJoiErrorParamName(joierr)
				const error = `Invalid param - ${field}`

				logger.warn(`Validation eror: ${error}`)

				res.status(400).json({ success: false, error })
				return next(error)
			}

			req.payload = payload
			return next()
		})
	}

	const keyAllowEmpty = `${key}AllowEmpty`
	exportObj[keyAllowEmpty] = (req, res, next) => {
		if (!(key in rules)) {
			const error = 'Validation rule not found'
			res.status(500).json({ success: false, error })
			return next(error)
		}

		const data = req.method === 'GET' ? req.query : req.body
		if (_.keys(data).length > 0) {
			return exportObj[key](req, res, next)
		}
		return next()
	}
})

export default exportObj
