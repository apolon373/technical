import client from '../index'

function getFromRedis(keyName) {
	return new Promise((resolve, reject) => {
		client.hgetall(keyName, function (err, reply) {
			if (err !== 'OK' && err !== null) {
				reject(err)
			}
			resolve(reply)
		})
	})
}

function setDataToRedis(keyName, data) {
	return new Promise((resolve, reject) => {
		client.hmset(keyName, data, function (err, reply) {
			if (err !== 'OK' && err !== null) {
				reject(err)
			}
			resolve(reply)
		})
	})
}

export default {
	getFromRedis,
	setDataToRedis,
}
