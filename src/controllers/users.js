const Models = require('../models')
import logger from 'logops'
import userSrv from '../service/users'
import _ from 'lodash'

async function createUser(req, res, next) {
	const data = req.method === 'GET' ? req.query : req.body

	let result
	try {
		result = await Models.Users.create(data)
	} catch (e) {
		const error = 'Failed to create user'
		logger.error(error)
		res.status(500).json({ success: false, error })
		return next()
	}

	try {
		await userSrv.setDataToRedis(result.id, data)
	} catch (e) {
		const error = 'Failed to set data to redis'
		logger.error(error, e)
	}

	res.status(200).json({ success: true, result })
	return next()
}

async function getUserById(req, res, next) {
	let id = _.get(req.params, 'id', null)

	let result
	try {
		result = await userSrv.getFromRedis(id)
	} catch (e) {
		const err = 'failed to get from redis'
		logger.error(err, e)
	}

	if (!result) {
		try {
			result = await Models.Users.findOne({ where: { id } })
		} catch (e) {
			const err = 'failed to find users'
			logger.error(err, e)
			res.json({ code: 400, err })
			return next(err)
		}
	}

	res.status(200).json({ success: true, result })
	return next()
}

async function updateUser(req, res, next) {
	const data = req.method === 'GET' ? req.query : req.body
	let id = _.get(req.params, 'id', null)

	try {
		await userSrv.setDataToRedis(id, data)
	} catch (e) {
		const error = 'Failed to set data to redis'
		logger.error(error, e)
	}

	try {
		await Models.Users.update(data, {
			where: { id },
		})
	} catch (e) {
		const err = 'failed to update'
		logger.error(err, e)
		res.json({ code: 400, err })
		return next()
	}

	res.status(200).json({ success: true })
	return next()
}

export default {
	createUser,
	getUserById,
	updateUser,
}
