import express from 'express'

import validatorSrv from '../service/validator'
import usersCtrl from '../controllers/users'

const router = express.Router()

router.post('/', validatorSrv.createUser, usersCtrl.createUser)
router.get('/:id', usersCtrl.getUserById)
router.put('/:id', validatorSrv.updateUser, usersCtrl.updateUser)

export default router
