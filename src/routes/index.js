import express from 'express'

import usersRouter from './users'

const router = express.Router()

router.use('/users', usersRouter)

// Send 404 error if no routes found
router.use((req, res, next) => {
	if (!res.headersSent) {
		res.status(404).json({ success: false, error: 'Invalid url' })
	}

	return next()
})

export default router
