'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
	class Users extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	Users.init(
		{
			full_name: DataTypes.STRING,
			date_of_birth: DataTypes.DATE,
			gender: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: 'Users',
		}
	)
	return Users
}
