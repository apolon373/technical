const env = process.env.NODE_ENV || 'development'
import pack from '../package.json'

const config = {
	development: {
		username: 'postgres',
		password: '',
		database: 'postgres',
		host: process.env.HOST || 'localhost',
		dialect: 'postgres',
		port: 5432,
		port_listen: 8011,
		reqBody: {
			limit: '50mb',
			parameterLimit: 50000,
		},
		urlMount: process.env.URL_MOUNT || '/api',
		server: {
			name: process.env.SERVER_NAME || pack.name,
			version: process.env.SERVER_VERSION || pack.version,
		},
	},
}

module.exports = config[env]
